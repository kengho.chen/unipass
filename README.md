# 專案介紹

**Project Code:** unipass

這個服務提供使用者一個簡易的密碼管理機制，特色如下：
* 使用者利用唯一一組密碼，就可以針對不同網站產生不同的登入密碼。
* 使用者可利用本服務重新取回不同網站的登入密碼。
* 本服務資料庫不存放使用者的任何密碼，無資料外洩風險。

# 開發環境

### 作業系統:
- Linux 2.6.23 or later 
- MacOS 10 or later 
- Window 7 or later 

### 語言：
- Go 1.11

# 建置與執行

### 建置資料庫

1. 安裝 MySQL 5.7 以上版本
2. 建立 database (名稱: unipass)
3. 執行 SQL 內容 （db/unipass_init.sql）

### 建置執行檔

 1. 取得第三方模組

```
$ go get github.com/go-sql-driver/mysql
 
```

 2. 編譯原始碼

```
$ go build -o build/unipass-web src/*.go
 
```

 3. 打包

```
複製 web 至 build/web
複製 config 至 build/config
```

 4. 調整應用程式設定

**Config File:** build/config/default.conf
```
設置 DB 使用者與密碼
 
```
### 執行

```
$ cd build/
$ ./unipass-web
```
### 使用者介面

open the url in browser:
```
http://{domain:port}/
 
```

default local env:
```
http://localhost:9090/
 
```